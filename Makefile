#
# To build my report, just run `make`
#
# Based on the answer by Markus Kuhn:
# https://stackoverflow.com/questions/12343428/latex-reference-and-makefile
#

TEX=pdflatex -shell-escape

.PHONY: all figs tables clean report slides

.DELETE_ON_ERROR:


all: report slides

report: report.pdf

slides: slides.pdf

%.pdf: %.tex figs tables
	latexmk -pdf -pdflatex="$(TEX)" $*.tex

figs:
	$(MAKE) -C figures

tables:
	$(MAKE) -C tables

clean:
	$(MAKE) -C figures clean
	$(MAKE) -C tables clean
	latexmk -c
