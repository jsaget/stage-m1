\documentclass[table,xcdraw]{beamer}

\usepackage[utf8]{inputenc}
\usepackage[T1]{fontenc}
\usepackage[english]{babel}

\usepackage{amsmath,amssymb}

\usepackage{multirow}

\usepackage{minted}

\usepackage{graphicx}

\usepackage{url}
%\usepackage[hidelinks]{hyperref}

\newcommand{\urlfootnote}[1]{\footnote{\url{#1}}}

\usepackage{array}
\usepackage{tabularx}

\usepackage{setspace}

\usepackage{csquotes}

\usetheme{CambridgeUS}
\usecolortheme{wolverine}

\title[CEPHALOPODE]{-- CEPHALOPODE --\\
  \scriptsize
  Certified Energy-Preserving Hardware using Absolute Laziness\\
  to Optimize Power and Offer Defenses against Errors\\
  \normalsize
  Design of execution unit in\\
  IoT functional language processor
}
\author[Jules Saget]{Jules Saget\\
  \footnotesize
  under the direction of\\
  \normalsize
  Carl-Johan H. Seger
}
\institute[Octopi @ Chalmers]{Octopi Team, Chalmers Tekniska Högskola}
\date{March -- July 2020}

\begin{document}

\titlepage


\begin{frame}{Context}
  \begin{itemize}
    \item<1-> IoT is not secure
    \item<2-> But Octopi is here: software + hardware tools
    \item<3-> Focus on hardware: ALU
    \item<4-> IoT special needs: security and energy preservation rather than
      performance
  \end{itemize}
\end{frame}

\begin{frame}{Outline of the presentation}
  \setcounter{tocdepth}{2}
  \tableofcontents
\end{frame}

\section{Theory}

\subsection{Functional assembly code}

\begin{frame}{What does a functional program \emph{mean}?}
  \begin{minipage}{0.69\textwidth}
  \inputminted{OCaml}{code/simple.ml}

  \phantom{a}

  \uncover<2,3>{transformed into $(\lambda x . + 1 x) 3$}
  \end{minipage}
  \begin{minipage}{0.3\textwidth}
    \uncover<3>{
      \begin{figure}[h]
        \centering
        \includegraphics[width=\textwidth]{figures/lambda.pdf}
        \label{fig:lambda}
      \end{figure}
    }
  \end{minipage}
\end{frame}

\begin{frame}{Combinators}
  \begin{align*}
    S e f g & \rightarrow_\beta e g (f g)\\
    K e f   & \rightarrow_\beta e \\
    I e     & \rightarrow_\beta e
  \end{align*}

  \begin{figure}[h]
    \centering
    \includegraphics[width=\textwidth]{figures/ski_combinators.pdf}
    \label{fig:ski}
  \end{figure}
\end{frame}



\begin{frame}{Example : $(\lambda x . + 1 x)3 \rightarrow_{\beta^*}
    S (K (+ 1)) I 3$
  }
  \begin{figure}[h]
    \centering
    \only<1>{\includegraphics[width=\textwidth]{figures/gr_ex_0.pdf}}
    \only<2>{\includegraphics[width=\textwidth]{figures/gr_ex_1.pdf}}
    \only<3>{\includegraphics[width=\textwidth]{figures/gr_ex_2.pdf}}
    \only<4>{\includegraphics[width=\textwidth]{figures/gr_ex_3.pdf}}
    \only<5>{\includegraphics[width=\textwidth]{figures/gr_ex_4.pdf}}
    \label{fig:gr_ex}
  \end{figure}
\end{frame}

\subsection{Low-level data representation}

\begin{frame}{Types in CEPHALOPODE}
  \centering
  \begin{minipage}{0.45\textwidth}
    \centering
    \begin{itemize}
      \item Application node (@): \texttt{(addr, addr)}
      \item Combinator node (\emph{S}, \emph{K}, \emph{I})
      \item Primitive node (arithmetic operations, comparisons, logical
        operations)
      \item Indirection node (=): \texttt{addr}
      \item Integer node: \texttt{(int, addr)}
    \end{itemize}
  \end{minipage}
  \begin{minipage}{0.45\textwidth}
    \centering
    \begin{figure}[h]
      \centering
      \includegraphics[width=\textwidth]{figures/tree.pdf}
      \label{fig:tree}
    \end{figure}
  \end{minipage}
\end{frame}

\subsubsection{Arbitrary precision integers}

\begin{frame}{Arbitrary precision integers}
  \input{tables/arith_2cpl.tex}
  How to represent -42
  in binary (chunks of 3 bits)?
  \begin{itemize}
    \item<2-6> Write 42
      in binary: \texttt{0101010}
    \item<3-6> Perform 2's complement: \texttt{1010110}
    \item<4-6> Cut in chunks: \texttt{1·010·110}
    \item<5-6> Sign extend (if needed): \texttt{111·010·110}
  \end{itemize}
  \uncover<6>{
    Here, -42 is stored in address \texttt{0x1}:
    \begin{minipage}{\textwidth}
      \centering
      \begin{tabular}[h]{c|c|c}
        \texttt{0x1}                & \texttt{0x2}              & \texttt{0x3} \\
        \texttt{(INT, 110, 0x2)}    & \texttt{(INT, 010, 0x3)}  & \texttt{(INT, 111, 0x0)}
      \end{tabular}
    \end{minipage}
  }
\end{frame}

\section[ALU]{Designing the ALU}

\begin{frame}{Module interface}
  \begin{figure}[h]
    \centering
    \includegraphics[width=\textwidth]{figures/tam.png}
    \label{fig:tam}
  \end{figure}
\end{frame}

%\subsection{\emph{“Simple”} units}
\subsection{Comparison Unit}

%\begin{frame}{Logic Unit}
%  NOT, AND, OR, COND (if then else)\\
%
%  \phantom{CO}
%  \begin{minipage}{0.8\textwidth}
%    \begin{itemize}
%      \item<2-5>[NOT] straightforward
%      \item<3-5>[AND] \texttt{AND A B} is \texttt{FALSE} if \texttt{NOT A} else
%        \texttt{B}
%      \item<4-5>[OR] \texttt{OR A B} is \texttt{TRUE} if \texttt{A} else
%        \texttt{B}
%      \item<5>[COND] \texttt{COND A B C} is \texttt{B} if \texttt{A} else
%        \texttt{C}
%    \end{itemize}
%  \end{minipage}
%\end{frame}

\begin{frame}{Comparison Unit}
  Operations: $=$, $\neq$, $\geq$, $>$
  \begin{figure}[h]
    \centering
    \includegraphics[width=\textwidth]{figures/comparator.pdf}
    \label{fig:comparator_fsm}
  \end{figure}
\end{frame}

\subsection{Addition Unit}

\begin{frame}{Addition FSM}
  \begin{figure}[h]
    \centering
    \includegraphics[width=0.65\textwidth]{figures/add_fsm}
    \label{fig:add_fsm}
  \end{figure}
\end{frame}

\begin{frame}{1 + 2}
  \begin{minipage}{0.34\textwidth}
    \texttt{001 + 010}:
    \begin{itemize}
      \item<2-7> Read \texttt{A}: \texttt{001}
      \item<3-7> Read \texttt{B}: \texttt{010}
      \item<4-7> Add \texttt{A} and \texttt{B}: \texttt{011}
      \item<5-7> Go into final mode: everything is fine
      \item<6-7> Write the result: \texttt{011}
      \item<7>   Done!
    \end{itemize}
  \end{minipage}
  \begin{minipage}{0.65\textwidth}
    \begin{figure}[h]
      \centering
      \only<1>{\includegraphics[width=\textwidth]{figures/add_fsm_idle.pdf}}
      \only<2>{\includegraphics[width=\textwidth]{figures/add_fsm_ab_read_a.pdf}}
      \only<3>{\includegraphics[width=\textwidth]{figures/add_fsm_ab_read_b.pdf}}
      \only<4>{\includegraphics[width=\textwidth]{figures/add_fsm_ab_add.pdf}}
      \only<5>{\includegraphics[width=\textwidth]{figures/add_fsm_final_check.pdf}}
      \only<6>{\includegraphics[width=\textwidth]{figures/add_fsm_final_link.pdf}}
      \only<7>{\includegraphics[width=\textwidth]{figures/add_fsm_done.pdf}}
      \label{fig:add_ex_1}
    \end{figure}
  \end{minipage}
\end{frame}

\begin{frame}{1 + 3}
  \begin{minipage}{0.34\textwidth}
    \texttt{001+011}:
    \begin{itemize}
      \item<2-9> Read \texttt{A}: \texttt{001}
      \item<3-9> Read \texttt{B}: \texttt{011}
      \item<4-9> Add \texttt{A} and \texttt{B}: \texttt{100}
      \item<5-9> Go into final mode: there is a sign problem!
      \item<6-9> Allocate space for the next chunk
      \item<7-9> Write the first chunk: \texttt{·100}
      \item<8-9> Write the last chunk: \texttt{000}
      \item<9>   Done!
    \end{itemize}
  \end{minipage}
  \begin{minipage}{0.65\textwidth}
    \begin{figure}[h]
      \centering
      \only<1>{\includegraphics[width=\textwidth]{figures/add_fsm_idle.pdf}}
      \only<2>{\includegraphics[width=\textwidth]{figures/add_fsm_ab_read_a.pdf}}
      \only<3>{\includegraphics[width=\textwidth]{figures/add_fsm_ab_read_b.pdf}}
      \only<4>{\includegraphics[width=\textwidth]{figures/add_fsm_ab_add.pdf}}
      \only<5>{\includegraphics[width=\textwidth]{figures/add_fsm_final_check.pdf}}
      \only<6>{\includegraphics[width=\textwidth]{figures/add_fsm_final_alloc.pdf}}
      \only<7>{\includegraphics[width=\textwidth]{figures/add_fsm_final_write.pdf}}
      \only<8>{\includegraphics[width=\textwidth]{figures/add_fsm_final_last_write.pdf}}
      \only<9>{\includegraphics[width=\textwidth]{figures/add_fsm_done.pdf}}
      \label{fig:add_ex_2}
    \end{figure}
  \end{minipage}
\end{frame}

\begin{frame}{Final example: -34 + 2}
  \begin{minipage}{0.34\textwidth}
    \texttt{111·011·110 + 010}
    \begin{itemize}
      \item<2-12>  Read \texttt{A} and \texttt{B}, and add them: \texttt{000}
      \item<5-12>  Go into A mode: there is a carry!
      \item<6-12>  Allocate then write: \texttt{·000}
      \item<8-12>  Read \texttt{A}, propagate carry: \texttt{100}
      \item<10-12> Second check: nothing left to do!
      \item<11-12> Write the result to memory, \texttt{111·100}
      \item<12>    Done!
    \end{itemize}
  \end{minipage}
  \begin{minipage}{0.65\textwidth}
    \begin{figure}[h]
      \centering
      \only<1>{\includegraphics[width=\textwidth]{figures/add_fsm_idle.pdf}}
      \only<2>{\includegraphics[width=\textwidth]{figures/add_fsm_ab_read_a.pdf}}
      \only<3>{\includegraphics[width=\textwidth]{figures/add_fsm_ab_read_b.pdf}}
      \only<4>{\includegraphics[width=\textwidth]{figures/add_fsm_ab_add.pdf}}
      \only<5>{\includegraphics[width=\textwidth]{figures/add_fsm_a_check.pdf}}
      \only<6>{\includegraphics[width=\textwidth]{figures/add_fsm_a_alloc.pdf}}
      \only<7>{\includegraphics[width=\textwidth]{figures/add_fsm_a_write.pdf}}
      \only<8>{\includegraphics[width=\textwidth]{figures/add_fsm_a_read.pdf}}
      \only<9>{\includegraphics[width=\textwidth]{figures/add_fsm_a_add.pdf}}
      \only<10>{\includegraphics[width=\textwidth]{figures/add_fsm_a_check.pdf}}
      \only<11>{\includegraphics[width=\textwidth]{figures/add_fsm_a_link.pdf}}
      \only<12>{\includegraphics[width=\textwidth]{figures/add_fsm_done.pdf}}
      \label{fig:add_ex_3}
    \end{figure}
  \end{minipage}
\end{frame}

\subsubsection{Subtraction}

\begin{frame}{A small word on subtraction}
  \centering
  \Huge
  \texttt{A - B = A + (-B)}
\end{frame}

\subsection{Multiplication Unit}

\begin{frame}{Multiplcation FSM}
  \begin{figure}[h]
    \centering
    \includegraphics[width=\textwidth]{figures/mul_fsm.pdf}
    \label{fig:mul_fsm}
  \end{figure}
\end{frame}

%\begin{frame}{Example : $23 * 31$}
%  \begin{figure}[h]
%    \centering
%    \only<1>{\includegraphics[width=0.8\textwidth]{figures/mul_fsm_idle.pdf}}
%    \only<2>{\includegraphics[width=0.8\textwidth]{figures/mul_fsm_read_a.pdf}}
%    \only<3>{\includegraphics[width=0.8\textwidth]{figures/mul_fsm_read_b.pdf}}
%    \only<4>{\includegraphics[width=0.8\textwidth]{figures/mul_fsm_mul.pdf}}
%    \only<5>{\includegraphics[width=0.8\textwidth]{figures/mul_fsm_ready_res.pdf}}
%    \only<6>{\includegraphics[width=0.8\textwidth]{figures/mul_fsm_add.pdf}}
%    \only<7>{\includegraphics[width=0.8\textwidth]{figures/mul_fsm_ready_address.pdf}}
%    \only<8>{\includegraphics[width=0.8\textwidth]{figures/mul_fsm_alloc.pdf}}
%    \only<9>{\includegraphics[width=0.8\textwidth]{figures/mul_fsm_write.pdf}}
%    \only<10>{\includegraphics[width=0.8\textwidth]{figures/mul_fsm_add_loop_check.pdf}}
%    \only<11>{\includegraphics[width=0.8\textwidth]{figures/mul_fsm_ready_res.pdf}}
%    \only<12>{\includegraphics[width=0.8\textwidth]{figures/mul_fsm_add.pdf}}
%    \only<13>{\includegraphics[width=0.8\textwidth]{figures/mul_fsm_ready_address.pdf}}
%    \only<14>{\includegraphics[width=0.8\textwidth]{figures/mul_fsm_alloc.pdf}}
%    \only<15>{\includegraphics[width=0.8\textwidth]{figures/mul_fsm_write.pdf}}
%    \only<16>{\includegraphics[width=0.8\textwidth]{figures/mul_fsm_add_loop_check.pdf}}
%    \only<17>{\includegraphics[width=0.8\textwidth]{figures/mul_fsm_b_loop_check.pdf}}
%    \only<18>{\includegraphics[width=0.8\textwidth]{figures/mul_fsm_read_b.pdf}}
%    \only<19>{\includegraphics[width=0.8\textwidth]{figures/mul_fsm_mul.pdf}}
%    \only<20>{\includegraphics[width=0.8\textwidth]{figures/mul_fsm_ready_res.pdf}}
%    \only<21>{\includegraphics[width=0.8\textwidth]{figures/mul_fsm_read_res.pdf}}
%    \only<22>{\includegraphics[width=0.8\textwidth]{figures/mul_fsm_add.pdf}}
%    \only<23>{\includegraphics[width=0.8\textwidth]{figures/mul_fsm_ready_address.pdf}}
%    \only<24>{\includegraphics[width=0.8\textwidth]{figures/mul_fsm_write.pdf}}
%    \only<25>{\includegraphics[width=0.8\textwidth]{figures/mul_fsm_add_loop_check.pdf}}
%    \only<26>{\includegraphics[width=0.8\textwidth]{figures/mul_fsm_ready_res.pdf}}
%    \only<27>{\includegraphics[width=0.8\textwidth]{figures/mul_fsm_add.pdf}}
%    \only<28>{\includegraphics[width=0.8\textwidth]{figures/mul_fsm_ready_address.pdf}}
%    \only<29>{\includegraphics[width=0.8\textwidth]{figures/mul_fsm_alloc.pdf}}
%    \only<30>{\includegraphics[width=0.8\textwidth]{figures/mul_fsm_write.pdf}}
%    \only<31>{\includegraphics[width=0.8\textwidth]{figures/mul_fsm_add_loop_check.pdf}}
%    \only<32>{\includegraphics[width=0.8\textwidth]{figures/mul_fsm_b_loop_check.pdf}}
%    \only<33>{\includegraphics[width=0.8\textwidth]{figures/mul_fsm_a_loop_check.pdf}}
%    \only<34>{\includegraphics[width=0.8\textwidth]{figures/mul_fsm_read_a.pdf}}
%    \only<35>{\includegraphics[width=0.8\textwidth]{figures/mul_fsm_read_b.pdf}}
%    \only<36>{\includegraphics[width=0.8\textwidth]{figures/mul_fsm_mul.pdf}}
%    \only<37>{\includegraphics[width=0.8\textwidth]{figures/mul_fsm_ready_res.pdf}}
%    \only<38>{\includegraphics[width=0.8\textwidth]{figures/mul_fsm_read_res.pdf}}
%    \only<39>{\includegraphics[width=0.8\textwidth]{figures/mul_fsm_add.pdf}}
%    \only<40>{\includegraphics[width=0.8\textwidth]{figures/mul_fsm_ready_address.pdf}}
%    \only<41>{\includegraphics[width=0.8\textwidth]{figures/mul_fsm_write.pdf}}
%    \only<42>{\includegraphics[width=0.8\textwidth]{figures/mul_fsm_add_loop_check.pdf}}
%    \only<43>{\includegraphics[width=0.8\textwidth]{figures/mul_fsm_ready_res.pdf}}
%    \only<44>{\includegraphics[width=0.8\textwidth]{figures/mul_fsm_read_res.pdf}}
%    \only<45>{\includegraphics[width=0.8\textwidth]{figures/mul_fsm_add.pdf}}
%    \only<46>{\includegraphics[width=0.8\textwidth]{figures/mul_fsm_ready_address.pdf}}
%    \only<47>{\includegraphics[width=0.8\textwidth]{figures/mul_fsm_write.pdf}}
%    \only<48>{\includegraphics[width=0.8\textwidth]{figures/mul_fsm_add_loop_check.pdf}}
%    \only<49>{\includegraphics[width=0.8\textwidth]{figures/mul_fsm_b_loop_check.pdf}}
%    \only<50>{\includegraphics[width=0.8\textwidth]{figures/mul_fsm_read_b.pdf}}
%    \only<51>{\includegraphics[width=0.8\textwidth]{figures/mul_fsm_mul.pdf}}
%    \only<52>{\includegraphics[width=0.8\textwidth]{figures/mul_fsm_ready_res.pdf}}
%    \only<53>{\includegraphics[width=0.8\textwidth]{figures/mul_fsm_read_res.pdf}}
%    \only<54>{\includegraphics[width=0.8\textwidth]{figures/mul_fsm_add.pdf}}
%    \only<55>{\includegraphics[width=0.8\textwidth]{figures/mul_fsm_ready_address.pdf}}
%    \only<56>{\includegraphics[width=0.8\textwidth]{figures/mul_fsm_write.pdf}}
%    \only<57>{\includegraphics[width=0.8\textwidth]{figures/mul_fsm_add_loop_check.pdf}}
%    \only<58>{\includegraphics[width=0.8\textwidth]{figures/mul_fsm_ready_res.pdf}}
%    \only<59>{\includegraphics[width=0.8\textwidth]{figures/mul_fsm_add.pdf}}
%    \only<60>{\includegraphics[width=0.8\textwidth]{figures/mul_fsm_ready_address.pdf}}
%    \only<61>{\includegraphics[width=0.8\textwidth]{figures/mul_fsm_write.pdf}}
%    \only<62>{\includegraphics[width=0.8\textwidth]{figures/mul_fsm_add_loop_check.pdf}}
%    \only<63>{\includegraphics[width=0.8\textwidth]{figures/mul_fsm_b_loop_check.pdf}}
%    \only<64>{\includegraphics[width=0.8\textwidth]{figures/mul_fsm_a_loop_check.pdf}}
%    \only<65>{\includegraphics[width=0.8\textwidth]{figures/mul_fsm_done.pdf}}
%    \label{fig:mul_ex}
%  \end{figure}
%  \def\oldtabcolsep{\tabcolsep}
%  \setlength{\tabcolsep}{4pt}
%  \tiny
%  \only<1>{\input{tables/mul_2by2_0.tex}}
%  \only<2>{\input{tables/mul_2by2_0.tex}}
%  \only<3>{\input{tables/mul_2by2_0.tex}}
%  \only<4-5>{\input{tables/mul_2by2_1.tex}}
%  \only<6-7>{\input{tables/mul_2by2_2.tex}}
%  \only<8>{\input{tables/mul_2by2_3.tex}}
%  \only<9-11>{\input{tables/mul_2by2_4.tex}}
%  \only<12-13>{\input{tables/mul_2by2_5.tex}}
%  \only<14>{\input{tables/mul_2by2_6.tex}}
%  \only<15-17>{\input{tables/mul_2by2_7.tex}}
%  \only<18>{\input{tables/mul_2by2_8.tex}}
%  \only<19-20>{\input{tables/mul_2by2_9.tex}}
%  \only<21>{\input{tables/mul_2by2_10.tex}}
%  \only<22-23>{\input{tables/mul_2by2_11.tex}}
%  \only<24-26>{\input{tables/mul_2by2_12.tex}}
%  \only<27-28>{\input{tables/mul_2by2_13.tex}}
%  \only<29>{\input{tables/mul_2by2_14.tex}}
%  \only<30-33>{\input{tables/mul_2by2_15.tex}}
%  \only<34>{\input{tables/mul_2by2_16.tex}}
%  \only<35>{\input{tables/mul_2by2_17.tex}}
%  \only<36-37>{\input{tables/mul_2by2_18.tex}}
%  \only<38>{\input{tables/mul_2by2_19.tex}}
%  \only<39-40>{\input{tables/mul_2by2_20.tex}}
%  \only<41-43>{\input{tables/mul_2by2_21.tex}}
%  \only<44>{\input{tables/mul_2by2_22.tex}}
%  \only<45-46>{\input{tables/mul_2by2_23.tex}}
%  \only<47-49>{\input{tables/mul_2by2_24.tex}}
%  \only<50>{\input{tables/mul_2by2_25.tex}}
%  \only<51-52>{\input{tables/mul_2by2_26.tex}}
%  \only<53>{\input{tables/mul_2by2_27.tex}}
%  \only<54-55>{\input{tables/mul_2by2_28.tex}}
%  \only<56-58>{\input{tables/mul_2by2_29.tex}}
%  \only<59-60>{\input{tables/mul_2by2_30.tex}}
%  \only<61-64>{\input{tables/mul_2by2_31.tex}}
%  \only<65>{\input{tables/mul_2by2_32.tex}}
%  \setlength{\tabcolsep}{\oldtabcolsep}
%\end{frame}

\section*{}

\begin{frame}
  \centering
  \LARGE
  Demo time!
\end{frame}

\begin{frame}{Thank you for listening!}
  \centering
  \LARGE
  Any questions?
\end{frame}

\end{document}
