add_fsm_states="idle done ab_read_a ab_read_b ab_add ab_alloc ab_write a_check a_link a_alloc a_write a_read a_add b_check b_link b_alloc b_write b_read b_add final_check final_link final_alloc final_write final_last_write"

for state in $add_fsm_states
do
    echo $state
    python generate_fig.py add_fsm $state
    make add_fsm_$state.pdf
done
