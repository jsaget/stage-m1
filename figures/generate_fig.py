import sys

argument_list = sys.argv

def is_target_def(line, target):
    return (((target+" ") in line or (target+';' in line)) and ';' in line and not ("->" in line or "rank" in line))

def add_color(def_line): # Always add red
    if ']' in def_line:
        [begining, end] = def_line.split(']')
        new_def_line = begining + ", color = red]" + end
        return new_def_line
    else:
        [begining, end] = def_line.split(';')
        new_def_line = begining + "[style = filled, fillcolor = white, color = red];" + end
        return new_def_line



base_filename = argument_list[1].split(".gv")[0]

target_name = argument_list[2]

target_filename = base_filename + '_' + target_name.lower()
target = target_name.upper()

base_file = open(base_filename+".gv","rt")

target_file = open(target_filename+".gv", "wt")

for line in base_file:
    if is_target_def(line, target):
        target_file.write(add_color(line))
    else:
        target_file.write(line)

base_file.close()
target_file.close()
