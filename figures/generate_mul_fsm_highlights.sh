mul_fsm_states="idle done read_a a_loop_check read_b mul b_loop_check ready_res read_res add ready_address alloc write add_loop_check"

for state in $mul_fsm_states
do
    echo $state
    python generate_fig.py mul_fsm $state
    make mul_fsm_$state.pdf
done
