"Test"

NUMBER_OF_LINES = 5

start_tokens = ["\\begin", "\\centering", "multi", "size"]
start_lines = []

end_tokens = ["\\end", "\\caption", "\\label"]
end_lines = []

BASE_FILE_NAME = "mul_2by2"
EXTENSION = ".tex"

FILENUM = 0

out_files = []

base_file = open(BASE_FILE_NAME + EXTENSION, "rt")

def has_a_member(test_list, source):
    """
    test_list : list
    source : list
    return type : bool
    Tests wether a member of test_list is in source
    """
    if test_list == []:
        return False
    elt = test_list[-1]
    if elt in source:
        return True
    return has_a_member(test_list[:-1], source)

N = 0

for line in base_file:
    if has_a_member(start_tokens, line):
        start_lines.append(line)
    elif has_a_member(end_tokens, line):
        end_lines.append(line)
    else:
        new_out_file = open(BASE_FILE_NAME+'_'+str(FILENUM)+EXTENSION,"wt")
        FILENUM+=1
        for sl in start_lines:
            new_out_file.write(sl)
        out_files.append(new_out_file)
        if N < NUMBER_OF_LINES:
            N += 1
        for out_file in out_files[-N:]:
            out_file.write(line)

base_file.close()

for out_file in out_files:
    for el in end_lines:
        out_file.write(el)
    out_file.close()
